<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Answer\AnswerRepositoryInterface;
use Illuminate\Http\Request;

class AnswerController extends Controller
{

    protected $answerRepository;

    public function __construct(AnswerRepositoryInterface $answerRepository)
    {
        $this->answerRepository = $answerRepository;
    }

    public function store(Request $request)
    {

        $data = [
            'question_id'=>$request->get('question_id'),
            'code'=>$request->get('code'),
            'content'=>$request->get('content'),
            'translate'=>$request->get('translate'),
            'is_correct'=>$request->get('is_correct'),
        ];

        $answer = $this->answerRepository->create($data);

        return [
            'success'=>true,
            'data'=> [
                'id'=>$answer->id,
                'content'=>$answer->content
            ],
            'message'=>'Create Answer Success'
        ];

    }

}
