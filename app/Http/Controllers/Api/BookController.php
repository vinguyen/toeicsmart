<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Book\BookRepositoryInterface;
use Illuminate\Http\Request;

/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="API Documentation TOEIC SMART",
 *     @OA\Contact(
 *         email="vinguyen.dev@gmail.com"
 *     ),
 * )
 */

class BookController extends Controller
{

    protected $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

  /**
   * @OA\GET(
   *     path="/books",
   *     tags={"Book"},
   *     description="Thông tin sách",
   *   @OA\Response(response=200, description="successful operation"),
   * )
   */

    public function index(Request $request)
    {
        $books = $this->bookRepository->getBooks();

        $name = $request->get('name');

        if (!empty($name)) {
            $books = $this->bookRepository->getBookByName($name);
        }

        return response()->json($books,200);

    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        $data = [
            'name'=>$request->get('name'),
            'status'=>$request->get('status'),
            'image'=>'',
            'year'=>$request->get('year'),
        ];

        $book = $this->bookRepository->create($data);

        return [
            'success' => true,
            'data' =>[
                'id'=>$book->id,
                'name'=>$book->name,
            ],
            'message'=>'Upload Book Success'
        ];

    }


    public function show($id)
    {
        $book = $this->bookRepository->find($id);

        return response()->json($book, 200);

    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
