<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Exam\ExamRepository;
use App\Repositories\PartType\PartTypeRepository;
use http\Env\Response;
use Illuminate\Http\Request;

class ExamController extends Controller
{

    protected $examRepository;

    public function __construct(ExamRepository $examRepository)
    {
        $this->examRepository = $examRepository;
    }

    public function index(Request $request)
    {
        $book_id = $request->get('book_id');

        if($book_id) {
            return $this->examRepository->getExamsByBookId($book_id);
        }

        return $this->examRepository->getExams();
    }

  public function getExamPartFive($part_id)
  {

    $data = $this->examRepository->getExamPartFive($part_id);

    return response()->json($data,200);

  }

  public function getExamPartOne($part_id)
  {
    $data = $this->examRepository->getExamPartOne($part_id);

    return response()->json($data, 200);

  }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data = [
            'name'=>$request->get('name'),
            'book_id'=>$request->get('book_id'),
            'url_audio'=>$request->get('url_audio'),
            'status'=>$request->get('status')
        ];

        $exam = $this->examRepository->create($data);

        return [
            'success' => true,
            'data' =>[
                'id'=>$exam->id,
                'name'=>$exam->name,
            ],
            'message'=>'Upload Exam Success'
        ];

    }


    public function show($id)
    {
        $data = $this->examRepository->find($id);

        return response()->json($data,200);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {

      $data = [
        'name'=>$request->get('name'),
        'book_id'=>$request->get('book_id'),
        'url_audio'=>$request->get('url_audio'),
        'status'=>$request->get('status')
      ];

      $exam = $this->examRepository->update($id, $data);

      if ($exam) {
        return response()->json($exam,200);
      }

      return response()->json([],401);

    }


    public function destroy($id)
    {
        //
    }

  public function getExamParts($exam_id)
  {

    $data = $this->examRepository->getExamParts($exam_id);

    return response()->json($data,200);

  }
}
