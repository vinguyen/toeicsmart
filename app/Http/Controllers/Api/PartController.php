<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Part\PartRepositoryInterface;
use Illuminate\Http\Request;

class PartController extends Controller
{

  protected $partRepository;

  public function __construct(PartRepositoryInterface $partRepository)
  {
      $this->partRepository = $partRepository;
  }

  public function index()
  {
      return $this->partRepository->getAll();
  }

  public function store(Request $request)
  {

      $exam_id = $request->get('exam_id');
      $part_type_id = $request->get('part_type_id');

      $part = $this->partRepository->findPartByExamType($exam_id,$part_type_id);

      if (empty($part)) {
          $data = [
              'exam_id'=>$request->get('exam_id'),
              'part_type_id'=>$request->get('part_type_id'),
              'url_audio'=>$request->get('url_audio')
          ];

          $part = $this->partRepository->create($data);

          return [
              'success' => true,
              'data'=> [
                  'id'=>$part->id,
              ],
              'message'=>'Update Part Success'
          ];
      }

      return [
          'success' => true,
          'data'=> [
              'id'=>$part->id
          ],
          'message' => 'Part Already Exist'
      ];

  }

  public function findPartByExamType(Request $request)
  {
      $exam_id = $request->get('exam_id');
      $part_type_id = $request->get('part_type_id');

      $part = $this->partRepository->findPartByExamType($exam_id, $part_type_id);

      return [
          'data'=>$part
      ];

  }

  public function findExamByPartTypeId($part_type_id)
  {

    $data = $this->partRepository->findExamByPartTypeId($part_type_id);

    return response()->json($data,200);

  }

  public function show($id)
  {
    $part = $this->partRepository->find($id);

    $data = [
      'id'=>$id,
      'name'=>$part->part_type->name,
      'title'=>$part->part_type->title,
      'url_audio'=>$part->url_audio,
      'exam'=>$part->exam->name,
      'exam_id'=>$part->exam_id,
      'part_type_id'=>$part->part_type_id,
      'book'=>$part->exam->book->name
    ];

    return response()->json($data,200);

  }

  public function update(Request $request, $id)
  {
    $data = [
      'url_audio'=>$request->get('url_audio'),
      'exam_id'=>$request->get('exam_id'),
      'part_type_id'=>$request->get('part_type_id'),
    ];

    $part = $this->partRepository->update($id, $data);

    if ($part) {
      return response()->json($part,200);
    }

    return response()->json([],401);

  }

}
