<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Part\PartRepository;
use App\Repositories\PartFive\PartFiveRepositoryInterface;
use App\Repositories\Question\QuestionRepository;
use App\Repositories\Question\QuestionRepositoryInterface;
use App\Repositories\Answer\AnswerRepository;
use Illuminate\Http\Request;

class PartFiveController extends Controller
{

    protected $fiveRepository;
    protected $questionRepository;

    public function __construct(PartFiveRepositoryInterface $fiveRepository,QuestionRepositoryInterface $questionRepository)
    {
        $this->fiveRepository = $fiveRepository;
        $this->questionRepository = $questionRepository;
    }

    public function index()
    {
        return $this->fiveRepository->getAll();
    }

    public function store(Request $request)
    {

      $exam_id = $request->get('exam_id');
      $code_question = $request->get('code_question');
      $answers = $request->get('answers');
      $key = $request->get('key');
      $vocabularies = $request->get('vocabularies');
      $translate = $request->get('translate');
      $format = $request->get('format');
      $explain = $request->get('explain');
      $content = $request->get('content');

      $part = new PartRepository();
      $question = new QuestionRepository();

      $part_id = $part->findPartByExamType($exam_id, 5)->id;

      $question = $question->create([
        'code_question'=>$code_question,
        'content'=>$content,
        'translate'=>$translate
      ]);

      $answerRepo = new AnswerRepository();

      foreach ($answers as $answer) {
        $answerRepo->create([
          'code'=>$answer['code'],
          'content'=>$answer['content'],
          'is_correct'=>$answer['code']==$key,
          'translate'=>$answer['translate'],
          'question_id'=>$question->id
        ]);
      }

      $partFive = $this->fiveRepository->create([
        'part_id'=>$part_id,
        'question_id'=>$question->id,
        'explain'=>$explain,
        'vocabularies'=>$vocabularies,
        'format'=>$format,
        'translate'=>$translate
      ]);

      return response()->json($partFive,200);

    }

    public function show($id)
    {
        return $this->fiveRepository->showDetailPartFive($id);
    }

}
