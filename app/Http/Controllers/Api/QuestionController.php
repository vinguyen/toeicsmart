<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Question\QuestionRepositoryInterface;
use Illuminate\Http\Request;

class QuestionController extends Controller
{

    protected $questionRepository;

    public function __construct(QuestionRepositoryInterface $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    public function store(Request $request)
    {
        $data = [
            'code_question'=>$request->get('code_question'),
            'content'=>$request->get('content'),
            'translate'=>$request->get('translate')
        ];

        $question = $this->questionRepository->create($data);

        return [
            'success'=>true,
            'data'=> [
                'id'=>$question->id,
            ],
            'message'=>'Create Question Success'
        ];

    }

    public function show($id)
    {
        $data = $this->questionRepository->showDetailQuestion($id);

        return [
            'data'=>$data
        ];
    }

}
