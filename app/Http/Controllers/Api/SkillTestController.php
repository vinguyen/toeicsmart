<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\SkillTest\SkillTestRepositoryInterface;
use Illuminate\Http\Request;

class SkillTestController extends Controller
{

    protected $skillTestRepository;

    public function __construct(SkillTestRepositoryInterface $skillTestRepository)
    {
        $this->skillTestRepository = $skillTestRepository;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
