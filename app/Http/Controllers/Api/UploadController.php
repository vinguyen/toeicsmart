<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadRequest;
use App\Repositories\Upload\UploadRepository;
use App\Repositories\Upload\UploadRepositoryInterface;

class UploadController extends Controller
{
  /**
   * @param UploadRequest $request
   * @return array|void
   */
  public function upload(UploadRequest $request)
  {

    $file = $request->file('file');

    if ($file) {
      /**
       * check image upload
       */

      if (Upload::isImage($file)) {
        list($fileName, $filePath) = Upload::move($file);

        /**
         * @var UploadRepositoryInterface $uploadRepository;
        */

        $uploadRepository = new UploadRepository();

        $upload = $uploadRepository->create([
          'name'=>$fileName,
          'url_file'=> $filePath,
          'file_type'=>'png'
        ]);

        return [
          'success'=>true,
          'url'=> $upload->url_file
        ];

      }

      if (Upload::isMusic($file)) {
        list($fileName, $filePath) = Upload::move($file);

        /**
         * @var UploadRepositoryInterface $uploadRepository;
         */

        $uploadRepository = new UploadRepository();

        $upload = $uploadRepository->create([
          'name'=>$fileName,
          'url_file'=> $filePath,
          'file_type'=>'mp3'
        ]);

        return [
          'success'=>true,
          'data'=> [
            'path'=>$filePath,
            'id'=>$upload->id,
            'url_file'=>$upload->url_file
          ]
        ];

      }
    }

    return $request;

  }

}
