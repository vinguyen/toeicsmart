<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\UserSkillTest\UserSkillTestRepositoryInterface;
use Illuminate\Http\Request;

class UserSkillTestController extends Controller
{

    protected $userSkillTestRepository;

    public function __construct(UserSkillTestRepositoryInterface $userSkillTestRepository)
    {
        $this->userSkillTestRepository = $userSkillTestRepository;
    }


    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
