<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        return $next($request)
//            ->header("Access-Control-Allow-Origin", "*")
//            ->header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
//          ->header("Access-Control-Allow-Credentials",  true)
//            ->header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Token-Auth, Authorization");



//      header("Access-Control-Allow-Origin: *");
//
//      // ALLOW OPTIONS METHOD
//      $headers = [
//        'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
//        'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin'
//      ];
//      if($request->getMethod() == "OPTIONS") {
//        // The client-side application can set only headers allowed in Access-Control-Allow-Headers
//        return Response::make('OK', 200, $headers);
//      }
//
//      $response = $next($request);
//      foreach($headers as $key => $value)
//        $response->header($key, $value);
//      return $response;

      return $next($request)
        ->header("Access-Control-Expose-Headers", "Access-Control-*")
        ->header("Access-Control-Allow-Headers", "Access-Control-*, Origin, X-Requested-With, Content-Type, Accept")
        ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, HEAD')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Allow', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');
    }
}
