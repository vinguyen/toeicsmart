<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartOne extends Model
{
  protected $fillable = [
    'part_id',
    'question_id',
    'url_audio',
    'url_image'
  ];

  public function question()
  {
    return $this->belongsTo('App\Models\Question');
  }

}
