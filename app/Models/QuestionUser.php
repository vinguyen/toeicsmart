<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionUser extends Model
{
    protected $fillable = [
        'question_id',
        'user_id',
        'answer_id'
    ];
}
