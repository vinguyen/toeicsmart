<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkillTest extends Model
{

    protected $fillable = [
        'total_user_tests',
        'part_id'
    ];

}
