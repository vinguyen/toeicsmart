<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSkillTest extends Model
{

    protected $fillable = [
        'user_id',
        'skill_test_id',
        'correct_sentences',
        'correct_ratio'
    ];

}
