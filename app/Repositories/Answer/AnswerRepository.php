<?php

namespace App\Repositories\Answer;

use App\Models\Answer;
use App\Repositories\BaseRepository;

class AnswerRepository extends BaseRepository implements AnswerRepositoryInterface
{

    public function getModel()
    {
        return Answer::class;
    }

    public function findAnswerByQuestionId($question_id)
    {

        return $this->model->where('question_id',$question_id)->get();

    }

}
