<?php

namespace App\Repositories\Book;

use App\Models\Book;
use App\Repositories\BaseRepository;

class BookRepository extends BaseRepository implements BookRepositoryInterface
{

    public function getModel()
    {
        return Book::class;
    }

    public function getBooks()
    {
        return $this->model->select('*')->take(5)->get();
    }

    public function getBookByName($name)
    {
        return $this->model->where('name',$name)->first();
    }
}
