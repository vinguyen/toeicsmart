<?php

namespace App\Repositories\Book;

interface BookRepositoryInterface
{

    public function getBooks();

    public function getBookByName($name);

}
