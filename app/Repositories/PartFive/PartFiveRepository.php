<?php

namespace App\Repositories\PartFive;

use App\Models\PartFive;
use App\Repositories\BaseRepository;
use App\Repositories\Question\QuestionRepository;

class PartFiveRepository extends BaseRepository implements PartFiveRepositoryInterface
{

    public function getModel()
    {
        return PartFive::class;
    }

    public function showDetailPartFive($id)
    {

        $part_five = $this->find($id);

        if ($part_five) {
            $question = new QuestionRepository();
            $question = $question->showDetailQuestion($part_five->question_id);

            $part_five = [
                "id"=>$question["id"],
                "code_question"=>$question["code_question"],
                "content"=>$question["content"],
                "answers"=>$question["answers"],
                "key"=>$question["key"],
                "explain"=>$part_five->explain,
                "vocabularies"=>$part_five->vocabularies,
                "translate"=>$part_five->translate,
                "format"=>$part_five->format
            ];

            return $part_five;
        }

        return [];

    }

}
