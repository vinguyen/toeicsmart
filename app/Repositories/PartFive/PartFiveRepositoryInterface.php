<?php

namespace App\Repositories\PartFive;

interface PartFiveRepositoryInterface
{
    public function showDetailPartFive($id);
}
