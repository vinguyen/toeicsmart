<?php

namespace App\Repositories\PartOne;

interface PartOneRepositoryInterface
{

    public function showDetailPartOne($id);

}
