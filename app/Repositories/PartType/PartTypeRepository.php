<?php

namespace App\Repositories\PartType;

use App\Models\PartType;
use App\Repositories\BaseRepository;

class PartTypeRepository extends BaseRepository implements PartTypeRepositoryInterface
{

  public function getModel()
  {
    return PartType::class;
  }

  public function getTitleName()
  {

    return $this->model->select(['id','name','title','url'])->get();

  }
}
