<?php

namespace App\Repositories\Question;

use App\Models\Question;
use App\Repositories\Answer\AnswerRepository;
use App\Repositories\BaseRepository;

class QuestionRepository extends BaseRepository implements QuestionRepositoryInterface
{

    public function getModel()
    {
        return Question::class;
    }

    public function showDetailQuestion($question_id)
    {
        $question = $this->find($question_id);

        $key = $question->answers()->select("code")->where("is_correct",1)->first();

        $question->answers = $question->answers()->select(["code","content","translate"])->orderBy('code','asc')->get();

        $question = [
            "id"=>$question_id,
            "code_question"=>$question->code_question,
            "content"=>$question->content,
            "translate"=>$question->translate,
            "key"=>$key->code,
            "answers"=>$question->answers
        ];

        return $question;
    }
}
