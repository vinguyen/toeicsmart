<?php

namespace App\Repositories\SkillTest;

use App\Models\SkillTest;
use App\Repositories\BaseRepository;

class SkillTestRepository extends BaseRepository implements SkillTestRepositoryInterface
{

    public function getModel()
    {
        return SkillTest::class;
    }
}
