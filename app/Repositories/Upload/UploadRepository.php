<?php

namespace App\Repositories\Upload;

use App\Models\Upload;
use App\Repositories\BaseRepository;

class UploadRepository extends BaseRepository implements UploadRepositoryInterface
{

  public function getModel()
  {
    return Upload::class;
  }
}
