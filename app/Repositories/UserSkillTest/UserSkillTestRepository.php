<?php

namespace App\Repositories\UserSkillTest;

use App\Models\UserSkillTest;
use App\Repositories\BaseRepository;

class UserSkillTestRepository extends BaseRepository implements UserSkillTestRepositoryInterface
{
    public function getModel()
    {
        return UserSkillTest::class;
    }
}
