<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('part_types');
        Schema::create('part_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('title');
            $table->text('direction');
            $table->string('url');
            $table->integer('number_question');
            $table->string('url_audio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('part_types');
    }
}
