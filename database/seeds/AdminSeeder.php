<?php

use Database\traits\DisableForeignKeys;
use Database\traits\TruncateTable;
use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('admins');

        $admins = [
            [
                'name'=>'Admin',
                'email'=>'admin@gmail.com',
                'email_verified_at' => now(),
                'password'=>bcrypt('admin'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('admins')->insert($admins);

        $this->enableForeignKeys();
    }
}
