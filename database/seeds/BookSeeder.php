<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $books = [
          [
              'name'=>'ETS 2018',
              'image'=>'',
              'status'=>1,
              'year'=>2018,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
          ],
          [
            'name'=>'ETS 2019',
            'image'=>'',
            'status'=>1,
            'year'=>2019,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ],
          [
            'name'=>'ETS 2020',
            'image'=>'',
            'status'=>1,
            'year'=>2020,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
          ]
        ];

        DB::table('books')->insert($books);

    }
}
