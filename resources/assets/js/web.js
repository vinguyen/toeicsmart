window.onscroll = function() {scrollNavMenu()};

let navMenu = document.getElementById('nav-menu');

function scrollNavMenu() {
  if (window.pageYOffset > 0) {
    navMenu.classList.add("nav-menu-top");
  }
  else {
    navMenu.classList.remove("nav-menu-top");
  }
}
