import Dashboard from "../views/Dashboard";
import PartFive from "../views/part-fives/PartFive";
import CreatePartFive from "../views/part-fives/CreatePartFive";
import PartOne from "@/admin/views/part-ones/PartOne";
import CreatePartOne from "@/admin/views/part-ones/CreatePartOne";
import Exam from "@/admin/views/exams/Exam";
import CreateExam from "@/admin/views/exams/CreateExam";
import UpdateExam from "@/admin/views/exams/UpdateExam";
import DetailExam from "@/admin/views/exams/DetailExam";
import EditPartExam from "@/admin/views/exams/EditPartExam";

export const routes = [
  {
    path: '/',
    component: Dashboard
  },
  {
    path: '/part-fives',
    component: PartFive
  },
  {
    path: '/part-fives/create',
    component: CreatePartFive
  },
  {
    path: '/part-ones',
    component: PartOne
  },
  {
    path: '/part-ones/create',
    component: CreatePartOne
  },
  {
    path: '/exams',
    component: Exam,
  },
  {
    path: '/exams/create',
    component: CreateExam
  },
  {
    path: '/exams/update/:exam_id',
    component: UpdateExam
  },
  {
    path: '/exams/:exam_id',
    component: DetailExam
  },
  {
    path: '/exams/parts/:part_id',
    component: EditPartExam
  }

]
