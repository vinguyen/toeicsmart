module.exports = {
    DOMAIN_URL: 'http://toeicsmart.com',
    API_URL: 'http://api.toeicsmart.com',
    ADMIN_URL: 'http://admin.toeicsmart.com',
    API_TOEIC: 'https://apitoeic.an.r.appspot.com/',
    API_LOCAL: 'http://127.0.0.1:8000'
}
