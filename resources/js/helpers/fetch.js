import axios from 'axios';

import Notify from './notify';
const env = require('@/env');
// const domainApi = env.API_TOEIC;
const domainApi = env.API_LOCAL;
let token = localStorage.getItem('token');
// let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU5MzM5NDYyNywiZXhwIjoxNTkzMzk4MjI3LCJuYmYiOjE1OTMzOTQ2MjcsImp0aSI6ImRCa0FQSHNLM081NXp6VjAiLCJzdWIiOjIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.kBcinm-j2oAuKWTGG_CtcKdmlwXTCtGl0n-X1xo-h88';

axios.defaults.headers['common'].Accept = 'application/json';
if (token) {
  axios.defaults.headers['common'].Authorization = 'Bearer ' + token;
}
axios.defaults.withCredentials = false;

const getClient = (baseUrl = null) => {

    const options = {
        baseURL: baseUrl,
    };

    const client = axios.create(options);

    // Add a request interceptor
    client.interceptors.request.use(
        requestConfig => requestConfig,
        (requestError) => {
            // eslint-disable-next-line no-undef
            Raven.captureException(requestError);

            return Promise.reject(requestError);
        },
    );

    // Add a response interceptor
    client.interceptors.response.use(
        response => response,
        (error) => {
            if (error.response.status >= 500) {
                // eslint-disable-next-line no-undef
                Raven.captureException(error);
            }

            return Promise.reject(error);
        },
    );

    return client;
};



class Fetch {

    constructor(baseUrl = domainApi) {
        this.client = getClient(baseUrl);
    }
    get(url, conf = {}, notify = false) {
        return this.client.get(url, conf)
            .then(response => {
                if (notify) {
                    Fetch.successNotify(response);
                }
                return Promise.resolve(response);
            })
            .catch(error => {
                if (notify) {
                    Fetch.errorNotify(error);
                }
                return Promise.reject(error);
            });
    }

    delete(url, conf = {}, notify = false) {
        return this.client.delete(url, conf)
            .then(response => {
                if (notify) {
                    Fetch.successNotify(response);
                }
                return Promise.resolve(response);
            })
            .catch(error => {
                if (notify) {
                    Fetch.errorNotify(error);
                }
                return Promise.reject(error);
            });
    }

    head(url, conf = {}, notify = false) {
        return this.client.head(url, conf)
            .then(response => {
                if (notify) {
                    Fetch.successNotify(response);
                }
                return Promise.resolve(response);
            })
            .catch(error => {
                if (notify) {
                    Fetch.errorNotify(error);
                }
                return Promise.reject(error);
            });
    }

    options(url, conf = {}, notify = false) {
        return this.client.options(url, conf)
            .then(response => {
                if (notify) {
                    Fetch.successNotify(response);
                }
                return Promise.resolve(response);
            })
            .catch(error => {
                if (notify) {
                    Fetch.errorNotify(error);
                }
                return Promise.reject(error);
            });
    }

    post(url, data = {}, conf = {}, notify = false) {
        return this.client.post(url, data, conf)
            .then(response => {
                if (notify) {
                    Fetch.successNotify(response);
                }
                return Promise.resolve(response);
            })
            .catch(error => {
                if (notify) {
                    Fetch.errorNotify(error);
                }
                return Promise.reject(error);
            });
    }

    put(url, data = {}, conf = {}, notify = false) {
        return this.client.put(url, data, conf)
            .then(response => {
                if (notify) {
                    Fetch.successNotify(response);
                }
                return Promise.resolve(response);
            })
            .catch(error => {
                if (notify) {
                    Fetch.errorNotify(error);
                }
                return Promise.reject(error);
            });
    }

    patch(url, data = {}, conf = {}, notify = false) {
        return this.client.patch(url, data, conf)
            .then(response => {
                if (notify) {
                    Fetch.successNotify(response);
                }
                return Promise.resolve(response);
            })
            .catch(error => {
                if (notify) {
                    Fetch.errorNotify(error);
                }
                return Promise.reject(error);
            });
    }

    static errorNotify(error) {
        let mess = error.response && error.response.data && error.response.data.message ? error.response.data.message : ''
        if (mess) {
            Notify.error(mess)
        } else {
            Notify.error()
        }
        let errors = error.response && error.response.data && error.response.data.errors ? error.response.data.errors : []
        for (let i = 0; i < errors.length; i++) {
            Notify.error(errors[i])
        }
    }

    static warningNotify(mess) {
        Notify.warn(mess);
    }

    static successNotify() {
        Notify.success();
    }

    static paymentSuccess() {
        Notify.paymentSuccess();
    }
}

export { Fetch };

