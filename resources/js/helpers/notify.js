import { Notification } from 'element-ui';
class Notify
{
    constructor (message, type = 'success', title = '') {
        this.message = message;
        this.type = type;
        if (title) {
            this.title = title;
        }
    }

    show () {
        let title = 'Thông báo';
        if (this.title || false) {
            title = this.title;
        } else {
            switch (this.type) {
                case 'success':
                    title = 'Thành công';
                    break;
                case 'warn':
                    title = 'Cảnh báo';
                    break;
                case 'error':
                    title = 'Có lỗi xảy ra';
                    break;
            }
        }
        Notification({
            title: title,
            message: this.message,
            type: this.type
        });
    }

    static success (message) {
        Notification({
            title: 'Thành công',
            message: message,
            type: 'success'
        });
    }

  static paymentSuccess (message) {
    Notification({
      title: 'Thanh toán thành công',
      message: message,
      type: 'success'
    });
  }

    static warn (message) {
        Notification({
            title: 'Cảnh báo',
            message: message,
            type: 'warning'
        });
    }

    static error (message) {
        Notification({
            title: 'Có lỗi xảy ra',
            message: message,
            type: 'error'
        });
    }

    static info (message) {
        Notification({
            title: 'Thông báo',
            message: message,
        });
    }
}


export default Notify;
