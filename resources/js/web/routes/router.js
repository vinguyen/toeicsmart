import Home from "@/web/views/Home";
import PartFive from "@/web/views/part-fives/PartFive";
import PartFiveTest from "@/web/views/part-fives/PartFiveTest";
import PartOne from "@/web/views/part-ones/PartOne";
import PartOneTest from "@/web/views/part-ones/PartOneTest";
import LogIn from "@/web/views/auth/LogIn";
import Register from "@/web/views/auth/Register";
import Profile from "@/web/views/users/Profile";
import ProfileUpdate from "@/web/views/users/ProfileUpdate";
import Statistic from "@/web/views/statistics/Statistic";

import store from "@/web/store";
import SocialLogin from "@/web/views/auth/SocialLogin";
import Payment from "@/web/views/payment/Payment";

const ifNotAuthenticated = (to, from, next) => {
  if(!store.getters["auth/isAuthenticated"]) {
    next()
    return
  }
  next('/');
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters["auth/isAuthenticated"]) {
    next()
    return
  }
  next('/login')
}

const ifAccountVip = (to, from, next) => {
  if (!store.getters["auth/isAuthenticated"]) {
    next('/login');
  }
  else if (store.getters["auth/isVip"]) {
    next();
    return;
  }
  next('/profile/payment')
}

export const routes = [
  {
    path: '/',
    component: Home,
    name: 'home'
  },
  {
    path: '/part-fives',
    component: PartFive,
    name: 'part-fives',
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/exams/part-fives/:part_id',
    component: PartFiveTest,
    name: 'exam-part-five',
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/part-ones',
    component: PartOne,
    name: 'part-ones',
    beforeEnter: ifAccountVip,
  },
  {
    path: '/exams/part-ones/:part_id',
    component: PartOneTest,
    name: 'exam-part-one',
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/login',
    component: LogIn,
    name: 'web-login',
    beforeEnter: ifNotAuthenticated,
  },
  {
    path: '/register',
    component: Register,
    name: 'web-register'
  },
  {
    path: '/profile',
    component: Profile,
    name: 'profile',
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/profile/update',
    component: ProfileUpdate,
    name: 'profileUpdate',
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/profile/statistics',
    component: Statistic,
    name: 'statistic',
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/auth/google/callback',
    component: SocialLogin,
    name: 'social'
  },
  {
    path: '/profile/payment',
    component: Payment,
    name: 'payment',
    beforeEnter: ifAuthenticated,
  }
];
