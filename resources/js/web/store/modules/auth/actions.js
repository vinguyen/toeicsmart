export default {
  setToken({commit}, value) {
    commit('SET_TOKEN', value);
  },
  setUserId({commit}, value) {
    commit('SET_USER_ID', value);
  },
  setRedirect({commit}, value) {
    commit('SET_REDIRECT',value);
  }
}
