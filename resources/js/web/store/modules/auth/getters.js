export default {
  token: (state) => state.token,
  userId: (state) => state.userId,
  redirect: (state) => state.redirect,
  isAuthenticated: (state) => !!state.token,
  isVip: (state) => state.isVip
}
