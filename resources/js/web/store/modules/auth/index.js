import actions from "@/web/store/modules/auth/actions";
import mutations from "@/web/store/modules/auth/mutations";
import getters from "@/web/store/modules/auth/getters";
import state from "@/web/store/modules/auth/state";

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
