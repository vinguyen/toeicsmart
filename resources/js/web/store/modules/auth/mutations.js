export default  {
  SET_TOKEN(state, value) {
    state.token = value;
  },
  SET_USER_ID(state, value) {
    state.userId = value;
  },
  SET_REDIRECT(state, value) {
    state.redirect = value;
  }
}
