export default {
  token: localStorage.getItem('token') || '',
  userId: null,
  redirect: null,
  isVip: localStorage.getItem('isVip')==='1'
}
