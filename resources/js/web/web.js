require('../bootstrap');

import Vue from 'vue';

import VueRouter from "vue-router";
import {routes} from "./routes/router";
import Element from 'element-ui';
import locale from 'element-ui/lib/locale/lang/vi';
import '../assets/plugins/elementui/index.css';
import store from "@/web/store";

Vue.use(VueRouter);
Vue.use(Element,{locale});

const router = new VueRouter({
    mode: 'history',
    routes
})

const web = new Vue({
  el: '#web',
  store,
  router
});
