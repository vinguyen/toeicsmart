@extends('admin.layouts.app')

@section('body_class','nav-md')
@section('page')
    <router-view></router-view>
@stop

@section('styles')
    {{ Html::style(mix('assets/admin/css/admin.css')) }}
@endsection

@section('scripts')
    {{ Html::script(mix('assets/admin/js/admin.js')) }}
@endsection
