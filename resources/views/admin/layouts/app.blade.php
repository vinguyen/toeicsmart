<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Love_Heart_symbol.svg/651px-Love_Heart_symbol.svg.png">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @meta

    {{ Html::style(mix('assets/app/css/app.css')) }}
    {{ Html::style(mix('css/app.css')) }}
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}

    @yield('styles')

    @yield('head')

</head>
<body class="@yield('body_class')">

<div id="app" style="height: 100%">
    @yield('page')
</div>

{{ Html::script(mix('assets/admin/js/dashboard.js')) }}
{{ Html::script(mix('js/admin.js')) }}

@tojs

@yield('scripts')
</body>
</html>
