@extends('admin.layouts.auth')

@section('body_class','login')

@section('content')
  <div>
    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">
          {{ Form::open(['route' => 'admin.login']) }}
          <h1>Đăng nhập</h1>

          <div>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                   placeholder="{{ __('views.auth.login.input_0') }}" required autofocus>
          </div>
          <div>
            <input id="password" type="password" class="form-control" name="password"
                   placeholder="{{ __('views.auth.login.input_1') }}" required>
          </div>

          @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
          @endif

          @if (!$errors->isEmpty())
            <div class="alert alert-danger" role="alert">
              {!! $errors->first() !!}
            </div>
          @endif

          <div>
            <button class="btn btn-primary submit" type="submit">Đăng nhập</button>
            <a class="reset_pass" href="/register">
              Bạn chưa có tài khoản?
            </a>
          </div>

          <div class="clearfix"></div>

          <br>
          <div class="separator">
            <span>Đăng nhập với</span>
            <div>
              <a href="" class="btn btn-success btn-facebook">
                <i class="fa fa-facebook"></i>
                Facebook
              </a>
              <a href="" class="btn btn-danger btn-google">
                <i class="fa fa-google"></i>
                Google
              </a>
            </div>
          </div>

          {{ Form::close() }}
        </section>
      </div>
    </div>
  </div>
@endsection

@section('styles')
  @parent

  {{ Html::style(mix('assets/auth/css/login.css')) }}
@endsection
