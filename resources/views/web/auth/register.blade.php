@extends('admin.layouts.auth')

@section('body_class','login')

@section('content')
  <div>
    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">
          {{ Form::open(['route' => 'admin.login']) }}
          <h1>Đăng ký</h1>

          <div>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                   placeholder="Họ tên" required autofocus>
          </div>

          <div>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                   placeholder="Email Address" required autofocus>
          </div>

          <div>
            <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}"
                   placeholder="Số điện thoại" required autofocus>
          </div>

          <div>
            <input id="password" type="password" class="form-control" name="password"
                   placeholder="Mật khẩu" required>
          </div>

          <div>
            <input id="password-reset" type="password" class="form-control" name="password-reset"
                   placeholder="Nhập lại mật khẩu" required>
          </div>

          @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
          @endif

          @if (!$errors->isEmpty())
            <div class="alert alert-danger" role="alert">
              {!! $errors->first() !!}
            </div>
          @endif

          <div>
            <button class="btn btn-primary submit" type="submit">Đăng ký</button>
            <a class="reset_pass" href="/login">
              Bạn đã có tài khoản?
            </a>
          </div>

          <div class="clearfix"></div>

          <br>
          <div class="separator">
            <span>Đăng ký với</span>
            <div>
              <a href="" class="btn btn-success btn-facebook">
                <i class="fa fa-facebook"></i>
                Facebook
              </a>
              <a href="" class="btn btn-danger btn-google">
                <i class="fa fa-google"></i>
                Google
              </a>
            </div>
          </div>

          {{ Form::close() }}
        </section>
      </div>
    </div>
  </div>
@endsection

@section('styles')
  @parent

  {{ Html::style(mix('assets/auth/css/login.css')) }}
@endsection
