<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Admin')->group(function () {
   Route::get('/login','Auth\LoginController@index');
   Route::post('login','Auth\LoginController@login')->name('admin.login');
   Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
   Route::group(['middleware'=>['auth:admin']], function () {
       Route::view('/{any}', 'admin.index')
           ->where('any', '.*');
   });
});
