<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::middleware(['cors'])->group(function() {
    Route::resource('books', 'Api\BookController');
    Route::resource('exams', 'Api\ExamController');
    Route::get('exams/part-fives/{part_id}', 'Api\ExamController@getExamPartFive');
    Route::get('exams/part-ones/{part_id}', 'Api\ExamController@getExamPartOne');
    Route::get('exams/{exam_id}/parts', 'Api\ExamController@getExamParts');
    Route::get('parts/findByExamType','Api\PartController@findPartByExamType');
    Route::resource('parts','Api\PartController');
    Route::get('parts/exams/{part_type_id}','Api\PartController@findExamByPartTypeId');
    Route::resource('questions','Api\QuestionController');
    Route::resource('answers','Api\AnswerController');
    Route::resource('part-fives','Api\PartFiveController');
    Route::resource('part-ones','Api\PartOneController');
    Route::resource('part-types','Api\PartTypeController');
    Route::post('upload','Api\UploadController@upload');
});
