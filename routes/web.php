<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Web')->group(function () {
//    Route::get('/login', 'Auth\LoginController@index');
    Route::post('/login', 'Auth\LoginController@login')->name('web.login');
//    Route::get('/register', 'Auth\LoginController@register')->name('web.register');
    Route::view('/{any}', 'web.index')
        ->where('any', '.*');
});
